class App {
  String app_name;
  String sector;
  String developer;
  int year_won;

  App (this.app_name,this.sector, this.developer, this.year_won);

  void describe () {
    print("App name: $app_name\nSector: $sector \ndeveloper: $developer \nYear:$year_won");
  }

  void toCapital() {
    print(app_name.toUpperCase());
  }

}
void main () {
  App my_app =
  new App ("Shyft","Standard Bank","Banking", 2012);

  my_app.describe();
  my_app.toCapital();
}